﻿using AMCTracker.App_Code.BusinessObjects;
using AMCTracker.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.Exchange.WebServices.Data;

namespace MTALeadScraping
{
    public class MTAEmailer
    {
        HttpClient _client = new HttpClient();
       public  MTAEmailer(HttpClient client)
        {
            _client = client;
        }
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public async Task<string> SendWelcomeEmail(List<LeadData> ParseData)
        {

            List<EmailData> lstEmailData = new List<EmailData>();
            lstEmailData = GetRecepitantsForEmail(ParseData);
            //lstEmailData.Add( new EmailData() { Email = "guna@americanmedicalcare.com", Name ="Gunarathinam", ReceivedOn = "01-mar-2018", Treatment = "GENERAL CHECK-UP"});
            foreach (var data in lstEmailData)
            {
                data.IsEmailSent = ComposeSendEmail(data);
                Logger.Info(string.Format("Welcome Email Sent for - {0}", JsonConvert.SerializeObject(data)));
            }
            string resMessage = await UpdateEmailContacts(lstEmailData);
            return resMessage;
        }

        private async Task<string> UpdateEmailContacts(List<EmailData> lstEmailData)
        {
            MTAEmailContactsResponse oEMailContacts = await GetEmailContacts();
            foreach (var data in lstEmailData.Where(x=> x.IsEmailSent == true))
            {
                oEMailContacts.EmailContacts.Add(new BOMailContact()
                {
                    Email = data.Email,
                    Name = data.Name,
                    ReceivedOn = data.ReceivedOn,
                    Treatment = data.Treatment,
                    WelcomeEmailSentOn = DateTime.Now.ToString(),
                    WelcomeEmailSentOnEpoch = DateTime.Now.ToEpoch()
                });
           }
            string returnMsg = await UpdateMTAEmailContacts(oEMailContacts.EmailContacts);
            return returnMsg;
        }

        private async Task<MTAEmailContactsResponse> GetEmailContacts()
        {
            MTAEmailContactsResponse emailContacts  = null;
            try
            {
                MTAEmailContactsRequest oReq = new MTAEmailContactsRequest()
                {
                    AccessToken = ""
                };
                HttpResponseMessage response = await _client.PostAsJsonAsync("MTALeadScrap/GetMTAEmailContacts/", oReq);
                response.EnsureSuccessStatusCode();

                emailContacts= await response.Content.ReadAsAsync<MTAEmailContactsResponse>();
            }
            catch (Exception ex)
            {   
                Logger.Error(ex);
            }
            return emailContacts;
        }

        private async Task<string> UpdateMTAEmailContacts(List<BOMailContact> lstEmailContacts)
        {
            string returnmsg = string.Empty;
            MTAEmailContactsUpdateRequest oReq = new MTAEmailContactsUpdateRequest()
            {
                AccessToken = "",
                EmailContacts = lstEmailContacts
            };

            HttpResponseMessage response = await _client.PostAsJsonAsync("MTALeadScrap/UpdateMTAEmailContacts/", oReq);
            response.EnsureSuccessStatusCode();

            MTAEmailContactsUpdateResponse ores = await response.Content.ReadAsAsync<MTAEmailContactsUpdateResponse>();
            returnmsg = ores.Message;

            Logger.Info("Update welcome mail response - " + returnmsg);

            return returnmsg;          

        }

        private bool ComposeSendEmail(EmailData data)
        {
            bool returnvalue = true;
            try
            {
                EmailHelper oEmailHelper = new EmailHelper();

                List<System.Net.Mail.Attachment> olstAttachements = new List<System.Net.Mail.Attachment>();
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\amclogo.jpg", "Cid_AMC_Logo"));
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\AnneSignature.gif", "Cid_AnneSignature"));

                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\AnneSignature.gif", "Cid_AnneSignature"));
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\facebook.png", "Cid_facebook"));
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\twitter.jpg", "Cid_twitter"));
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\googleplus.png", "Cid_googleplus"));
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\linkedin.png", "Cid_linkedin"));
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\instagram.jpg", "Cid_instagram"));

                oEmailHelper.SendEmail(ConfigurationManager.AppSettings["WelcomeMailSubject"],
                    data.Email,
                    GetEmailContent(data.Name, data.Treatment, "welcome"),
                    olstAttachements,
                    ConfigurationManager.AppSettings["WelcomeMailFromAddress"]
                    );
            }
            catch (Exception Ex)
            {
                returnvalue = false;
                Logger.Debug(string.Format("EmailID : {0}, Name: {1}, ReceivedOn: {3}", data.Email, data.Name, data.ReceivedOn));
                Logger.Error(Ex);
            }
            return returnvalue;
        }
        private string GetEmailContent(string sPatientName, string sprocedurename, string type)
        {
            string emailContent = string.Empty;
            sprocedurename = sprocedurename.Trim() == string.Empty ? "Treatment" : sprocedurename;
            if (type == "welcome")
            {
                emailContent = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\LeadWelcomeEmail.html");
                emailContent = emailContent.Replace("#PatientName#", sPatientName);
                emailContent = emailContent.Replace("#ProcedureName#", sprocedurename);
            }
            else //followup 
            {
                emailContent = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\LeadFollowUpEmail.html");
                emailContent = emailContent.Replace("#PatientName#", sPatientName);
                emailContent = emailContent.Replace("#ProcedureName#", sprocedurename);

            }
            return emailContent;

        }
        private List<EmailData> GetRecepitantsForEmail(List<LeadData> ParseData)
        {
            List<EmailData> lstEmailDt = new List<EmailData>();
            foreach (var data in ParseData.Select(i => new { i.Email, i.Name,i.ReceivedOn})
                      .Distinct()
                     .ToArray())
            {
                lstEmailDt.Add(new EmailData()
                {
                    Email = data.Email,
                    Name = data.Name,
                    Treatment = ParseData.Where(x => x.Email == data.Email
                                                    && x.Name == data.Name).First().Treatment,
                    IsEmailSent = false,
                    ReceivedOn = data.ReceivedOn
                });
            }
            return lstEmailDt;
        }

        public async Task<string> SendFollowUpEmail()
        {
            MTAEmailContactsResponse oEMailContacts = await GetEmailContacts();
            List<BOMailContact> oTempList = oEMailContacts.EmailContacts;                   
            int followEmailThresoldEpoch = DateTime.Now.AddDays(-2).Date.ToEpoch();
            foreach (var data in oEMailContacts.EmailContacts.ToList().Where(x => x.WelcomeEmailSentOnEpoch <= followEmailThresoldEpoch))
            {
                if (checkEmailOnAMCServer(data))
                {
                    oTempList.Remove(data);
                    Logger.Info("Follow-up Email, Mail exists in AMC Server and ignoring follow-up email. Info - " + JsonConvert.SerializeObject(data));
                }
                else
                {
                  if(ComposeSendFollowEmail(data))
                    {
                        oTempList.Remove(data);
                        Logger.Info("Follow-up Email Sent. Info - " + JsonConvert.SerializeObject(data));
                    }
                }
            }

            string returnMsg = await UpdateMTAEmailContacts(oTempList);
            Logger.Info("Follow-Email Update Contact response msg - " + returnMsg);
            return returnMsg;
        }

        public bool checkEmailOnAMCServer(BOMailContact data)
        {
           // data.Email = "guna@americanmedicalcare.com";
            bool returnValue = false;
            try
            {
                ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
                service.Credentials = new WebCredentials(ConfigurationManager.AppSettings["Email_UserAccount"]
                    ,ConfigurationManager.AppSettings["Email_Password"]
                    ,ConfigurationManager.AppSettings["EMail_Domain"]);

                service.TraceEnabled = true;
                service.TraceFlags = TraceFlags.All;

                service.AutodiscoverUrl("care@americanmedicalcare.com", RedirectionUrlValidationCallback);
                
                var inboxitems = service.FindItems(           
                    new FolderId(WellKnownFolderName.Inbox, new Mailbox(ConfigurationManager.AppSettings["Email_UserAccount"])),
                                new SearchFilter.SearchFilterCollection(LogicalOperator.And, new SearchFilter[] {
                                        new SearchFilter.IsGreaterThanOrEqualTo(ItemSchema.DateTimeReceived, DateTime.Now.AddDays(-2)),
                                         new SearchFilter.IsEqualTo(EmailMessageSchema.From, data.Email)}), new ItemView(15));

                /*
                 ExtendedPropertyDefinition PR_TRANSPORT_MESSAGE_HEADERS = new ExtendedPropertyDefinition(0x007D, MapiPropertyType.String);
                 SearchFilter HeaderFilter = new SearchFilter.ContainsSubstring(PR_TRANSPORT_MESSAGE_HEADERS, data.Email, ContainmentMode.Substring, ComparisonMode.IgnoreCaseAndNonSpacingCharacters);

                 FindItemsResults<Item> senditems = service.FindItems(
                     new FolderId(WellKnownFolderName.SentItems, new Mailbox("care@americanmedicalcare.com")),
                                 new SearchFilter.SearchFilterCollection(LogicalOperator.And, new SearchFilter[] {
                                 new SearchFilter.IsGreaterThanOrEqualTo(ItemSchema.DateTimeSent, DateTime.Now.AddDays(-2))                                
                                 //ew SearchFilter.Exists(EmailMessageSchema.ToRecipients, new  List<EmailAddress>(){ new EmailAddress("guna@amerianmedicalcare.com") })
                                 }), new ItemView(15));

                 senditems.Where(x=> x.ExtendedProperties.);
                 */

                //returnValue = inboxitems.Count() > 0  || senditems.Count() > 0 ? true : false;
                returnValue = inboxitems.Count() > 0 ? true : false;
            }
            catch(Exception Ex)
            {
                returnValue = false;
                Logger.Info("Error on checking mail exists for follow-up Email." + JsonConvert.SerializeObject(data));
                Logger.Error(Ex);
            }
            return returnValue;
        }
        private  bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;

            Uri redirectionUri = new Uri(redirectionUrl);

            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }
        private bool ComposeSendFollowEmail(BOMailContact data)
        {
            bool returnvalue = true;
            try
            {
                EmailHelper oEmailHelper = new EmailHelper();

                List<System.Net.Mail.Attachment> olstAttachements = new List<System.Net.Mail.Attachment>();
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\amclogo.jpg", "Cid_AMC_Logo"));
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\AnneSignature.gif", "Cid_AnneSignature"));

                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\AnneSignature.gif", "Cid_AnneSignature"));
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\facebook.png", "Cid_facebook"));
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\twitter.jpg", "Cid_twitter"));
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\googleplus.png", "Cid_googleplus"));
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\linkedin.png", "Cid_linkedin"));
                olstAttachements.Add(oEmailHelper.GetMailAttachment(AppDomain.CurrentDomain.BaseDirectory + @"\Templates\images\instagram.jpg", "Cid_instagram"));

                oEmailHelper.SendEmail(ConfigurationManager.AppSettings["FollowUpMailSubject"],
                    data.Email,
                    GetEmailContent(data.Name, data.Treatment, "followup"),
                    olstAttachements,
                    ConfigurationManager.AppSettings["FollowUpMailFromAddress"]
                    );
            }
            catch (Exception Ex)
            {
                returnvalue = false;
                Logger.Debug("Error on sending Follow-Up Mail: "+ JsonConvert.SerializeObject(data));
                Logger.Error(Ex);
            }
            return returnvalue;
        }
    }

    public static class Extensions
    {
        public static int ToEpoch(this DateTime date)
        {
            if (date == null) return int.MinValue;
            DateTime epoch = new DateTime(2017, 6, 1);
            TimeSpan epochTimeSpan = date - epoch;
            return (int)epochTimeSpan.TotalSeconds;
        }

        public static DateTime FromEpoch(this int epoch)
        {
            return new DateTime(2017, 6, 1).AddSeconds(epoch);
        }
    }
}
