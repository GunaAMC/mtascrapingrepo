﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MTALeadScraping
{
    public class EmailHelper
    {
        public bool SendEmail(string subject, string toaddress, string content, List<Attachment> lstAttachment, string fromAddress)
        {
            bool returnflag = true;
            try
            {

                using (MailMessage mm = new MailMessage(fromAddress, toaddress))
                {
                    mm.Subject = subject;
                    mm.Body = content;
                    //mm.CC.Add(new MailAddress(ConfigManager.Email.CCAddress));
                    mm.IsBodyHtml = true;

                    foreach (var item in lstAttachment)
                    {
                        mm.Attachments.Add(item);
                    }
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = ConfigurationManager.AppSettings["Email_SMTPHost"];
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(ConfigurationManager.AppSettings["Email_UserAccount"],
                           ConfigurationManager.AppSettings["Email_Password"]);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = int.Parse(ConfigurationManager.AppSettings["Email_Port"]);

                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["Email_CanSend"]))
                        smtp.Send(mm);

                    
                }
            }
            catch (Exception ex)
            {
                returnflag = false;
                throw ex;
            }
            return returnflag;
        }
        public Attachment GetMailAttachment(string imagepath, string contentID)
        {
            Attachment oAttachment = new Attachment(imagepath);
            oAttachment.ContentId = contentID;
            oAttachment.ContentDisposition.Inline = true;
            oAttachment.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
            return oAttachment;
        }

        public void sendEmailGmailAPI()
        {
            // If modifying these scopes, delete your previously saved credentials
            // at ~/.credentials/gmail-dotnet-quickstart.json
             string[] Scopes = { GmailService.Scope.GmailReadonly };
             string ApplicationName = "Gmail API .NET Quickstart";

            UserCredential credential;

            using (var stream =
                new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/gmail-dotnet-quickstart.json");

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Gmail API service.
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            // Define parameters of request.
            UsersResource.LabelsResource.ListRequest request = service.Users.Labels.List("me");

            // List labels.
            IList<Label> labels = request.Execute().Labels;
            Console.WriteLine("Labels:");
            if (labels != null && labels.Count > 0)
            {
                foreach (var labelItem in labels)
                {
                    Console.WriteLine("{0}", labelItem.Name);
                }
            }
            else
            {
                Console.WriteLine("No labels found.");
            }
            Console.Read();



        }
    }
}
