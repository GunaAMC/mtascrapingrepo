﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AMCTracker.App_Code.BusinessObjects;
using AMCTracker.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.IO;
using System.Configuration;
using log4net;
using System.Net.Mail;

namespace MTALeadScraping
{
    public class MTALeadScraper
    {
       HttpClient _client = new HttpClient();
       string _sAPIURI = string.Empty;
       List<LeadData> ParseData = new List<LeadData>();
       int _currentPageNo = 1;
       bool _IsLastScrapRecordFound = false;
        MTAEmailer oMTAEMailer; 
       private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public  MTALeadScraper(HttpClient client)
        {
            _client = client;
            oMTAEMailer = new MTAEmailer(client);
            ParseData = new List<LeadData>();
        }

        public async Task ProcessScraping(string sAPIURI)
        {
            _sAPIURI = sAPIURI;
            MTALeadLastScrapResponse lastScrapdata = await getLastScrapDetails();

            await getMTALLeadData(lastScrapdata);


        }
        private async Task<MTALeadLastScrapResponse>  getLastScrapDetails()
        {
            MTALeadLastScrapResponse lastScrapdata = null;
            try
            {

                
                MTALeadLastScrapRequest oReq = new MTALeadLastScrapRequest()
                {
                    AccessToken = ""
                };

                HttpResponseMessage response = await _client.PostAsJsonAsync("MTALeadScrap/GetLastScrapDetails/", oReq);
                response.EnsureSuccessStatusCode();

                lastScrapdata = await response.Content.ReadAsAsync<MTALeadLastScrapResponse>();
            }
            catch(Exception ex)
            {
                LogConsole(ex.Message);
                Logger.Error(ex);
            }
            return lastScrapdata;
        }

        private async Task getMTALLeadData(MTALeadLastScrapResponse lastScrapdata)
        {
            DateTime dtStartTime = DateTime.Now;
            LogConsole("MTA Lead Scrapping Started ");
            LogConsole(string.Format("Last Scarp details, Email - {0}, Recevied On - {1}"
                            , lastScrapdata.LastScrapDet.LastScrapedEmail
                            , lastScrapdata.LastScrapDet.LastScrapedReceviedAt));
            // Initialize the Chrome Driver
            var chromeDriverService = ChromeDriverService.CreateDefaultService();
            chromeDriverService.HideCommandPromptWindow = true;
            var chromeOptions = new ChromeOptions();
            using (var driver = new ChromeDriver(chromeDriverService, chromeOptions))
            {
                // Go to the home page
                driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["MTALoginURL"]);

                // Get the page elements
                var userNameField = driver.FindElementById("MainContent_txtUserName");
                var userPasswordField = driver.FindElementById("MainContent_txtpPassword");
                var loginButton = driver.FindElementByXPath("//input[@value='Login']");

                // Type user name and password
                userNameField.SendKeys(ConfigurationManager.AppSettings["MTALoginName"]);
                userPasswordField.SendKeys(ConfigurationManager.AppSettings["MTALoginPassword"]);

                // and click the login button
                loginButton.Click();

                new SelectElement(driver.FindElement(By.XPath("//select[@name='ctl00$MainContent$ddlList']"))).SelectByValue("Last90");

                //To parse first page
                parsePage(driver, lastScrapdata);

                
                while (!_IsLastScrapRecordFound)
                {
                    IWebElement pagingLinkbutton = null;
                    if (_currentPageNo % 5 == 0)
                    {
                        ++_currentPageNo;
                        var elements = driver.FindElements(By.XPath("//a[text() ='...']"));
                        if (elements.Count > 1)
                            pagingLinkbutton = elements.Last();
                        else
                            pagingLinkbutton = elements.First();

                        if (pagingLinkbutton.Displayed)
                        {
                            pagingLinkbutton.Click();
                            parsePage(driver, lastScrapdata);
                        }
                    }
                    else
                    {
                        string pagingPath = string.Format("//a[text() ='{0}']", ++_currentPageNo);

                        pagingLinkbutton = driver.FindElement(By.XPath(pagingPath));
                        if (pagingLinkbutton.Displayed)
                        {
                            pagingLinkbutton.Click();
                            parsePage(driver, lastScrapdata);
                        }
                    }
                }
            }
            //upload data to tracker db
            await UploadDataAMC();

            //update last processed record in config           
            string uploadResponse = await UpdateLastProcessLeadData();
            Logger.Info("Update last processed records response" + uploadResponse);

            //string res = await oMTAEMailer.SendWelcomeEmail(ParseData);

           // string resp = await oMTAEMailer.SendFollowUpEmail();

            //Console.WriteLine("Result - {0}", result.GetAttribute("innerHTML"));


            DateTime dtCompletedTime = DateTime.Now;
            LogConsole(string.Format("Completed the scrapping in {0} min.", dtCompletedTime.Subtract(dtStartTime).Minutes.ToString()));
        }



        private void parsePage(ChromeDriver driver, MTALeadLastScrapResponse lastScrapdata)
        {
            var result = driver.FindElementByXPath("//div[@class='leaddsiplayitemmt']");
            if (result.Displayed)
            {
                processData(result, lastScrapdata);

                LogConsole(string.Format("Completed parsing the page# -{0}", _currentPageNo.ToString()));
                LogConsole(string.Format("Total number of records parsed - {0}", ParseData.Count));
            }
        }

        //private bool checkStopCondition(MTALeadLastScrapResponse lastScrapdata)
        //{
        //    bool returnValue = true;

        //    returnValue = !ParseData.Exists(x => x.Email.Trim() == lastScrapdata.LastScrapDet.LastScrapedEmail
        //                                    && x.ReceivedOn.Trim() == lastScrapdata.LastScrapDet.LastScrapedReceviedAt);
        //    return returnValue;
        //}

        private void processData(IWebElement data, MTALeadLastScrapResponse lastScrapdata)
        {
            IReadOnlyCollection<IWebElement> result = data.FindElements(By.XPath("//div[@class='leaddsiplayitemmtrow']"));

            int i = 0;
            foreach (var item in result)
            {
                if (i > 0)
                { //logic to skip the header row 
                    if (!CheckIsLastScrapData(item, lastScrapdata))
                        parseRowData(item);
                    else
                    {
                        _IsLastScrapRecordFound = true;
                        break;
                    }
                        
                }
                i++;
            }

        }
        private bool CheckIsLastScrapData(IWebElement rowData, MTALeadLastScrapResponse lastScrapdata)
        {
            bool returnValue = false;
            LeadData oLeadData = new LeadData();
            parseContact(rowData.FindElement(By.ClassName("cellcontact")).Text, ref oLeadData);
            oLeadData.ReceivedOn = rowData.FindElement(By.ClassName("cellro")).Text;
            if (oLeadData.Email == lastScrapdata.LastScrapDet.LastScrapedEmail
                && oLeadData.ReceivedOn == lastScrapdata.LastScrapDet.LastScrapedReceviedAt)
                returnValue = true;

            return returnValue;
        }
        private void parseRowData(IWebElement rowData)
        {
            LeadData oLeadData = new LeadData();

            oLeadData.Name = rowData.FindElement(By.ClassName("cellname")).Text.Trim();

            parseContact(rowData.FindElement(By.ClassName("cellcontact")).Text , ref oLeadData );

            oLeadData.Country = rowData.FindElement(By.ClassName("cellcountry")).Text.Trim();
            oLeadData.ProcedureType = parseForCommaEscape(rowData.FindElement(By.ClassName("cellspec")).Text.Trim());
            oLeadData.Treatment = parseForCommaEscape(rowData.FindElement(By.ClassName("celltreatment")).Text.Trim());
            oLeadData.Comments = parseForCommaEscape(rowData.FindElement(By.ClassName("cellcomments")).Text.Trim());
            parsePrefLocation(rowData.FindElement(By.ClassName("cellpd")).Text, ref oLeadData);
            oLeadData.TentativeTimeFrame = rowData.FindElement(By.ClassName("celltt")).Text.Trim();
            oLeadData.DecidingFactor = rowData.FindElement(By.ClassName("celldf")).Text.Trim();
            oLeadData.ReceivedOn = rowData.FindElement(By.ClassName("cellro")).Text.Trim();            
            ParseData.Add(oLeadData);
        }
        private string parseForCommaEscape(string sData)
        {
            string sValue = sData;
            if (sData.Contains(","))
            {
                sValue = sData.Replace(",", "#");
            }
            return sValue; 
        }
        private void parseContact(string contactdet,  ref LeadData oLeadData)
        {
           
            string[] str = contactdet.Split(',');
            if (str.Length > 1)
            {
                oLeadData.Email = str[0].Trim();
                oLeadData.Phone= str[1].Trim();
            }
            else if(str.Length == 1)
                oLeadData.Email = str[0].Trim();

        }
        private void parsePrefLocation(string locationdet, ref LeadData oLeadData)
        {
            locationdet = locationdet.Replace("1-", "|").Replace("2-", "|").Replace("3-", "|");

            string[] split_values = locationdet.Split('|');
            List<string> pllocdet = new List<string>();
            for (int i = 1; i < split_values.Length; i++)
                pllocdet.Add(parseForCommaEscape(split_values[i]));

            if (pllocdet.Count == 3) {
                oLeadData.PreferredLocationOne = pllocdet[0].Trim();
                oLeadData.PreferredLocationTwo = pllocdet[1].Trim();
                oLeadData.PreferredLocationThree = pllocdet[2].Trim();
            }
            if (pllocdet.Count == 2)
            {
                oLeadData.PreferredLocationOne = pllocdet[0].Trim();
                oLeadData.PreferredLocationTwo = pllocdet[1].Trim();                
            }
            if (pllocdet.Count == 1)
            {
                oLeadData.PreferredLocationOne = pllocdet[0].Trim();                
            }            
        }

        private async Task UploadDataAMC()
        {
            if (ParseData.Count == 0)
                return;

            string file = ConvertObjectToCSVFile();

            var message = new HttpRequestMessage();
            var content = new MultipartFormDataContent();

            var filestream = new FileStream(file, FileMode.Open);
            var fileName = Path.GetFileName(file);
            content.Add(new StreamContent(filestream), "file", fileName);


            message.Method = HttpMethod.Post;
            message.Headers.Add("LeadSource", "MTA");
            message.Content = content;
            message.RequestUri = new Uri(_sAPIURI + "LeadQualify/UploadLeads");

            var res = await _client.SendAsync(message);
            res.EnsureSuccessStatusCode();
            LogConsole("Upload the Data successfully.");
            /*await _client.SendAsync(message).ContinueWith(task =>
             {
                 if (task.Result.IsSuccessStatusCode)
                 {
                     LogConsole("Upload the Data successfully.");
                 }
             });*/

        }

        private string ConvertObjectToCSVFile()
        {
            //string csvfilePath = "";

            var sb = new StringBuilder();
            // Add header in CSV file.
            sb.AppendLine("Name,Email,Phone,Country,ProcedureType,Treatment,Comments,PL1,PL2,PL3,Timeframe,DecidingFactor,ReceivedOn");
            foreach (var item in ParseData)
            {
                sb.AppendLine(item.Name + "," +
                              item.Email +"," +
                              item.Phone+ "," +
                              item.Country + "," +
                              item.ProcedureType + "," +
                              item.Treatment + "," +
                              item.Comments + "," +
                              item.PreferredLocationOne + ","+
                              item.PreferredLocationTwo + "," +
                              item.PreferredLocationThree + "," +
                              item.TentativeTimeFrame + "," +
                              item.DecidingFactor + "," +
                              item.ReceivedOn);
            }
            string currentExecutionPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string csvPath = currentExecutionPath + @"\export.csv";

            if (File.Exists(csvPath))
                File.Delete(csvPath);

            File.WriteAllText(csvPath, sb.ToString());

            return csvPath;
        }

        private async Task<string> UpdateLastProcessLeadData()
        {
            string returnmsg = string.Empty;
            try
            {
                if (ParseData.Count > 0)
                {

                    LeadData olastLeadData = ParseData.FirstOrDefault();

                    BOLeadData oLeadData = new BOLeadData()
                    {
                        LastScrapedEmail = olastLeadData.Email,
                        LastScrapedReceviedAt = olastLeadData.ReceivedOn
                    };

                    MTALeadLastScrapUpdateRequest oReq = new MTALeadLastScrapUpdateRequest()
                    {
                        AccessToken = "",
                        LastScrapDet = oLeadData
                    };

                    HttpResponseMessage response = await _client.PostAsJsonAsync("MTALeadScrap/UpdateLastScrapDetails/", oReq);
                    response.EnsureSuccessStatusCode();

                    MTALeadLastScrapUpdateResponse ores = await response.Content.ReadAsAsync<MTALeadLastScrapUpdateResponse>();

                    returnmsg = ores.Message;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
            return returnmsg;
        }

       
        private void LogConsole(string message)
        {  
            Console.WriteLine(message);
            Logger.Info(message);
        }
    }    
    public class LeadData
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Country { get; set; }
        public string ProcedureType { get; set; }
        public string Treatment { get; set; }
        public string Comments { get; set; }
        public string PreferredLocationOne { get; set; }
        public string PreferredLocationTwo { get; set; }
        public string PreferredLocationThree { get; set; }
        public string TentativeTimeFrame { get; set; }
        public string DecidingFactor { get; set; }
        public string ReceivedOn { get; set; }
    }  
    public class EmailData
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string ReceivedOn { get; set; }
        public string Treatment { get; set; }
        public bool IsEmailSent { get; set; }
    }
}

