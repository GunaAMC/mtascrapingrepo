﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;
using log4net;
using AMCTracker.App_Code.BusinessObjects;

namespace MTALeadScraping
{
    class Program
    {   

        static HttpClient client = new HttpClient();
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            // bool r =  new MTAEmailer(client).checkEmailOnAMCServer(new BOMailContact());

           // (new EmailHelper()).sendEmailGmailAPI();

            RunAsync().Wait();

            Console.ReadLine();
           
        }
        static async Task RunAsync()
        {
            string sAPIURI = ConfigurationManager.AppSettings["APIUri"]; 
            client.BaseAddress = new Uri(sAPIURI);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //https://docs.microsoft.com/en-us/aspnet/web-api/overview/advanced/calling-a-web-api-from-a-net-client

            try
            {
                MTALeadScraper oScraper = new MTALeadScraper(client);
               await oScraper.ProcessScraping(sAPIURI);
               
                //oScraper.SendWelcomeEmail();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Logger.Error(ex.Message);
            }
            
        }
    }
}
