﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace TwillioProcess
{
    class Program
    {
        static void Main(string[] args)
        {
            //GetCallLogs();
            DeleteRecordings();
        }

        static void DeleteRecordings()
        {
            string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"];
            string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"];

            TwilioClient.Init(accountSid, authToken);

            DateTime dtDeleteBefore = DateTime.UtcNow.AddMonths(-1);

            var recordings = RecordingResource.Read(
                dateCreatedBefore: dtDeleteBefore,
                pathAccountSid: accountSid
            );

            foreach (var record in recordings)
            {
                RecordingResource.Delete(
                       pathAccountSid: accountSid,
                       pathSid: record.Sid);

                //Console.WriteLine(record.Sid);
            }
        }
        static void GetCallLogs()
        {
            string accountSid = ConfigurationManager.AppSettings["TwilioAccountSid"];
            string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"];

            TwilioClient.Init(accountSid, authToken);

            DateTime dtCallsFrom = DateTime.UtcNow.AddDays(-10);

            var calls = CallResource.Read( 
                startTimeAfter: dtCallsFrom,               
                pathAccountSid: accountSid
            );

            List<CallDetail> callList = new List<CallDetail>();

            foreach (var record in calls)
            {               
                if (record.Status == CallResource.StatusEnum.Busy ||
                    record.Status == CallResource.StatusEnum.NoAnswer ||
                    record.Status == CallResource.StatusEnum.Completed
                    )
                {
                    callList.Add(
                        new CallDetail()
                        {
                            Date = record.StartTime,
                            ForwardedFrom = record.From,
                            From = record.From,
                            To = record.To,
                            Status = record.Status,
                            callSID = record.Sid,
                            parentSID = record.ParentCallSid
                        });

                    //Console.WriteLine("{0},{1},->{2},{3},{4},{5},{6}", record.StartTime, record.From, record.ForwardedFrom, record.To, record.Status, record.Sid, record.ParentCallSid);
                }
            }

            List<CallDetail> returnList = new List<CallDetail>();
            foreach( var call in callList)
            {
                if (call.Status == CallResource.StatusEnum.NoAnswer)
                {
                    string sFrom = string.Empty;
                    if(callList.Exists(x => x.callSID == call.parentSID && x.Status == CallResource.StatusEnum.Completed))
                    {
                        sFrom = callList.Where(x => x.callSID == call.parentSID && x.Status == CallResource.StatusEnum.Completed).First().From;
                    }

                    returnList.Add(
                        new CallDetail()
                        {
                            Date = call.Date,
                            From =  sFrom,
                            ForwardedFrom = call.From,
                            To = call.To
                        }
                       );
                    Console.WriteLine("{0},{1},{2},{3}", call.Date, sFrom, call.From, call.To);
                }
            }
        }

        public class CallDetail
        {
            public DateTime? Date { get; set; }
            public string ForwardedFrom { get; set; }
            public string From { get; set; }
            public string To { get; set; }
            public CallResource.StatusEnum Status { get; set; }
            public string callSID { get; set; }
            public string parentSID { get; set; }
        }
    }
}
